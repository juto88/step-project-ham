window.onscroll = function() {
  let navbar = document.querySelector('.navbar');
  let sticky = navbar.offsetTop;

  if (window.pageYOffset >= sticky) {
    navbar.classList.add('sticky');
  } else {
    navbar.classList.remove('sticky');
  }
};

const tabsNav = document.querySelector(".tabs-nav");
const tabsItems = document.querySelectorAll(".tabs_item");

tabsNav.addEventListener("click", onTabClickTabs);

function onTabClickTabs(event) {
  const currentBtn = event.target;
  if (!currentBtn.classList.contains("tabs_nav-btn")) return;

  const tabId = currentBtn.getAttribute("data-tab");
  const currentTab = document.querySelector(tabId);

  if (!currentBtn.classList.contains("active")) {
    Array.from(document.querySelectorAll(".tabs_nav-btn, .tabs_item")).forEach(function(item) {
      item.classList.remove("active");
    });

    currentBtn.classList.add("active");
    currentTab.classList.add("active");
  }
}

document.addEventListener('DOMContentLoaded', function() {
  // Массив объектов с категориями и изображениями
  let workItems = [
    {
      category: '#All',
      images: [
        'work image/Layer1.png',
        'work image/Layer2.png',
        'work image/Layer3.png',
        'work image/Layer4.png',
        'work image/Layer5.png',
        'work image/Layer6.png',
        'work image/Layer7.png',
        'work image/Layer8.png',
        'work image/Layer9.png',
        'work image/Layer10.png',
        'work image/Layer11.png',
        'work image/Layer12.jpg'
      ]
    },
    {
      category: '#Graphic_Design',
      images: [
        'work image/Layer1.png',
        'work image/Layer2.png',
        'work image/Layer3.png'
      ]
    },
    {
      category: '#Web_Design',
      images: [
        'work image/Layer4.png',
        'work image/Layer5.png',
        'work image/Layer6.png'
      ]
    },
    {
      category: '#Landing_Pages',
      images: [
        'work image/Layer7.png',
        'work image/Layer8.png',
        'work image/Layer9.png'
      ]
    },
    {
      category: '#Wordpress',
      images: [
        'work image/Layer10.png',
        'work image/Layer11.png',
        'work image/Layer12.jpg'
      ]
    }
  ];

  // Массив с дополнительными изображениями
  const additionalImages = [
    'work image/external/1.jpg',
    'work image/external/2.jpg',
    'work image/external/3.jpg',
    'work image/external/4.jpg',
    'work image/external/5.jpg',
    'work image/external/6.jpg',
    'work image/external/7.jpg',
    'work image/external/8.jpg',
    'work image/external/9.jpg',
    'work image/external/10.jpg',
    'work image/external/11.jpg',
    'work image/external/12.jpg',
  ];

  // Функция для отображения изображений по выбранной категории
  function showImagesByCategory(items, categoryId) {
    // Найти соответствующий объект категории
    const category = items.find(item => item.category === categoryId);

    // Найти контейнер для отображения изображений
    const workContent = document.querySelector('.work_content');

    // Очистить контейнер от предыдущих изображений
    workContent.innerHTML = '';

    // Создать и добавить новые элементы img с изображениями
    category.images.forEach(imageSrc => {
      const img = document.createElement('img');
      img.src = imageSrc;
      workContent.appendChild(img);
    });

    // Сделать активной выбранную кнопку категории
    const activeButton = document.querySelector('.work_nav-btn.active');
    if (activeButton) {
      activeButton.classList.remove('active');
    }
    const selectedButton = document.querySelector(`[data-tab="${categoryId}"]`);
    if (selectedButton) {
      selectedButton.classList.add('active');
    }
  }

  // Функция для отображения дополнительных изображений
  function showAdditionalImages() {
    const workContent = document.querySelector('.work_content');

    // Создать и добавить новые элементы img с дополнительными изображениями
    additionalImages.forEach(imageSrc => {
      const img = document.createElement('img');
      img.src = imageSrc;
      workContent.appendChild(img);
    });

    // Скрыть кнопку "LOAD MORE"
    const loadMoreButton = document.querySelector('.btn3');
    loadMoreButton.style.display = 'none';
  }

  // Получить все кнопки категорий
  const categoryButtons = document.querySelectorAll('.work_nav-btn');

  // Добавить обработчик события для каждой кнопки категории
  categoryButtons.forEach(button => {
    button.addEventListener('click', function() {
      const categoryId = button.getAttribute('data-tab');
      showImagesByCategory(workItems, categoryId);
    });
  });

  // Показать изображения из категории 'All' при загрузке страницы
  showImagesByCategory(workItems, '#All');

  // Обработчик события для кнопки "LOAD MORE"
  const loadMoreButton = document.querySelector('.btn3');
  loadMoreButton.addEventListener('click', showAdditionalImages);
});

$(document).ready(function(){
  $('.switch').slick({
    centerMode: true,
    centerPadding: '60px',
    slidesToShow: 4,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 4
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
  });
});
		













// const workNav = document.querySelector(".work_nav");
// const allWorkItems = document.querySelectorAll(".work_item");

// let activeTab = null;

// workNav.addEventListener("click", onTabClickWork);

// function onTabClickWork(event) {
//   const currentBtn = event.target;
//   if (!currentBtn.classList.contains("work_nav-btn")) return;

//   const tabId = currentBtn.getAttribute("data-tab");
//   const currentTab = document.querySelector(tabId);

//   if (currentBtn === activeTab) {
//     // Натиснута вже активна кнопка
//     return;
//   }

//   Array.from(allWorkItems).forEach(function (item) {
//     item.style.display = "none";
//   });

//   Array.from(document.querySelectorAll(".work_nav-btn")).forEach(function (item) {
//     item.classList.remove("active");
//   });

//   currentBtn.classList.add("active");
//   currentTab.classList.add("active");
//   currentTab.style.display = "";

//   activeTab = currentBtn;
// }


// document.addEventListener("DOMContentLoaded", function() {
//   Array.from(allWorkItems).forEach(function (item) {
//     if (item.id !== "All") {
//       const images = item.getElementsByTagName("img");
//       for (let j = 0; j < images.length; j++) {
//         images[j].style.display = "none";
//       }
//     }
//   });
// });

// function filterWorkContent(activeId) {
//   const items = activeId === '#All' ? document.querySelectorAll('.work_item#All') : document.querySelectorAll('.work_item');

//   items.forEach(function(item) {
//     const itemId = '#' + item.id;
//     const images = item.querySelectorAll('img');

//     if (itemId === activeId || activeId === '#All') {
//       item.style.display = '';
//       images.forEach(function(image) {
//         image.style.display = '';
//       });
//     } else {
//       item.style.display = 'none';
//       images.forEach(function(image) {
//         image.style.display = 'none';
//       });
//     }
//   });
// }

// const buttons = document.getElementsByClassName('work_nav-btn');
// for (let i = 0; i < buttons.length; i++) {
//   const button = buttons[i];
//   button.addEventListener('click', function() {
//     const dataTab = this.getAttribute('data-tab');
//     filterWorkContent(dataTab);
//   });
//   button.addEventListener('mouseover', function() {
//     // Заборонити hover ефект на неактивних кнопках
//     if (!this.classList.contains('active')) {
//       this.classList.remove('hover');
//     }
//   });
  
//   button.addEventListener('mouseout', function() {
//     // Відновити hover ефект на неактивних кнопках
//     if (!this.classList.contains('active')) {
//       this.classList.add('hover');
//     }
//   });
// }


